(function () {
    window.loadDB2WordCloud = function (self, extraConfigs) {
        // set config from extraConfigs or set defaults
        // default configs
        var userConfig = {
            maxWords: 200,
            targetSampleSize: 5000,
            ignoreWords: [],
            ignorePhrases: [],
            language: 'en',
            advanced: {
                getData: {
                    perPage: 500,
                },
                debug: /(e|E)xt=1/.test(window.location.search) // default automatically enable if DB in dev mode
            },
            highchartsConfigs: {
                series: [{
                    type: 'wordcloud',
                    name: 'Occurrences',
                    turboThreshold: 10000,
                    rotation: {
                        from: 0,
                        to: 0
                    },
                    minFontSize: 11,
                    maxFontSize: 40
                }],
                chart: {
                    marginRight: 0,
                    marginLeft: 0,
                    marginBottom: 0
                },
                title: {
                    text: ''
                },
                credits: {
                    enabled: false
                },
                tooltip: {
                    enabled: false
                }
            },
            theme: {
                opacityScale: [0.5, 1]
            }
        }
        $.extend(true, userConfig, extraConfigs || {}); // merge extraConfigs into userConfig (defaults)

        var container = self.$el.find(self._uiBindings.content); // element into which word cloud is inserted
        var containerID = 'wordcloud_' + self.$el.attr('chartid');
        var spinner = self.dataTile.ui.loading; // default DB2 chart spinner

        var stopwords = [];

        // supported languages - ISO code + regex to match the names
        var supportedLanguages = {
            "en": /^en$|english/i,
            "it": /^it$|italian|italiano/i
        };

        function setup() {
            spinner.show();

            debugLog(userConfig);

            // fetch all required resources
            // check for all resources first and only fetch if needed
    
            var resourcesToFetch = [];
    
            if ( // word cloud module is not present by default, check for it and fetch
                typeof Highcharts.seriesTypes['wordcloud'] == 'undefined' &&
                !window.gettingHighChartsWordCloud
            ) {
                resourcesToFetch.push(
                    $.getScript('https://code.highcharts.com/stock/7.0/modules/wordcloud.js')
                    .done(function () {
                        delete window.gettingHighChartsWordCloud;
                    })
                );
                resourcesToFetch.push(
                    $.getScript('https://cdnjs.cloudflare.com/ajax/libs/xregexp/3.1.1/xregexp-all.min.js')
                    .done(function () {
                        delete window.gettingHighChartsWordCloud;
                    })
                );
            }
    
            function getAutoLanguageCode() {
                var filters = chartTemplate.Filters();

                for (filter of filters) {
                    if (Array.isArray(filter.Filters)) {
                        for (field of filter.Filters) {
                            if (field.ScaleType == 4 &&
                                /language|[^\w]*lang[^\w]*/i.test(field.Name) &&
                                Array.isArray(field.Values) &&
                                field.Values.length
                            ) {
                                var sourceId = field.SourceID;
                                var attributeID = field.ID;
                                var valueId = field.Values[0].ID;
                                
                                getValueStrings(sourceId, attributeID, function(data) {
                                    var found = false;

                                    for (val of data) {
                                        if (val.id === valueId) {
                                            for (suppLang in supportedLanguages) {
                                                if (supportedLanguages[suppLang].test(val.name)) {
                                                    languageCode = suppLang;
                                                    found = true;

                                                    debugLog('found language "' + suppLang + '" in filter');

                                                    break;
                                                }
                                            }
                                        }

                                        if (found) break;
                                    }

                                    fetchStopwords();
                                });

                                return;
                            } 
                        }
                    }
                }

                fetchResources();
            }

            var languageCode = userConfig.language.toLowerCase();
            console.log('language code = ' + languageCode);
            if (languageCode === 'auto') {
                getAutoLanguageCode();
            } else {
                fetchStopwords();
            }

            function fetchStopwords() {
                //if (languageCode !== 'en') { // fetch list of stopwords for the configured language. By default "en" is hard-coded
                    $.ajax({
                        url: 'https://platform-services.custom.eu.mcx.cloud/DB2Extensibility/wordcloudStopwords/stopwords-' + languageCode + '.php',
                        success: function (data) {
                            try {
                                data = JSON.parse(data);
                                stopwords = data.stopwords;
                            } catch (e) {
                                console.error(
                                    'loadDB2WordCloud failed to decode stopwords for language "' + languageCode +
                                    '", defaulting to English ("en")'
                                );
                                console.error(e);
                            }

                            fetchResources();
                        },
                        error: function (e) {
                            console.error(
                                'loadDB2WordCloud failed to fetch stopwords for language "' + languageCode +
                                '", defaulting to English ("en")'
                            );
                            console.error(e);

                            fetchResources();
                        }
                    });
                // } else {
                //     stopwords = stopwords_en;

                //     fetchResources();
                // }
            }
        
            // fetch all resources then begin
            function fetchResources() {                
                debugLog('fetching ' + resourcesToFetch.length + ' resources');

                $.when(resourcesToFetch).then(function () {
                    debugLog('finished fetching resources');
        
                    // wait until HighCharts Word Cloud module has been loaded
                    if (typeof Highcharts.seriesTypes['wordcloud'] !== 'function') {
                        var waiting = setInterval(function () {
                            if (typeof Highcharts.seriesTypes['wordcloud'] == 'function') {
                                clearInterval(waiting);
        
                                main();
                            }
                            debugLog('waiting for wordcloud script to complete fetching (initiated by another wordcloud)');
                        }, 500); // try every half second
                    } else {
                        main();
                    }
                });
            }
        }

        function main() { // main Word Cloud script (after initial setup complete)
            debugLog('finished all setup');
            debugLog(self);

            var chartConfig;

            if (typeof window[containerID + '_data'] === 'undefined') { // wordcloud data not yet fetched
                // modify and force-save changes to dashboard report - extensibility widget needs to be "modified" to work with verbatim data fields
                getReport(function (report) {
                    var thisChart = getChartFromReport(report, self.model.get('GUID'));

                    description = JSON.parse(thisChart.Description);
                    if (!thisChart.Breakdowns || description.chartType !== 'table') {
                        // change this chart to column-only table with all required configs
                        var description = JSON.parse(thisChart.Description);

                        description.chartType = 'table';
                        thisChart.Description = JSON.stringify(description);
                        thisChart.Breakdowns = chartTemplate.Breakdowns();

                        saveReport(report, function (res) {
                            if (confirm('New WordCloud chart has been saved. The page must be reloaded to continue. Please click OK reload the page now')) {
                                window.location.reload();
                            } else {
                                console.warn('New WordCloud chart has been saved. Please reload the page now');
                            }
                        });
                    } else {
                        setupWordCloud();
                    }
                });
            } else {
                setupWordCloud();
            }

            function setupWordCloud() {
                debugLog('setting up word cloud');

                chartConfig = getFromTemplate(chartTemplate, self.model);
                delete chartConfig.Description;
                if (userConfig.advanced.debug) {
                    window[containerID + '_chartConfig'] = chartConfig;
                }

                // add ID to container
                container.attr('id', containerID);

                if (typeof window[containerID + '_data'] !== 'undefined') {
                    var newHash = stringToHash(JSON.stringify({
                        Flags: chartConfig.Flags,
                        SourceID: chartConfig.SourceID,
                        Children: chartConfig.Children,
                        Filters: chartConfig.Filters,
                        BeginDate: chartConfig.BeginDate,
                        EndDate: chartConfig.EndDate
                    }));

                    if (newHash == window[containerID + '_data'].hash) {
                        totalData = window[containerID + '_data'].data;
                        createWordCloud(totalData);
                        return;
                    }
                }

                // get all verbatim data as per current filters and date range
                var totalData = [];
                var perPage = userConfig.advanced.getData.perPage;
                var completedRequests = 0;
                var totalPages = 1;
                var interval = 1;

                // first getData call gets the total number of responses - used to calculate totalPages
                getData({
                    request: chartConfig,
                    page: 1,
                    pageSize: perPage,
                    success: function (data) {
                        if (data.C <= userConfig.targetSampleSize) {
                            interval = 1;
                        } else {
                            interval = Math.ceil(data.C / userConfig.targetSampleSize);
                        }
                        totalPages = Math.ceil(data.C / (interval * perPage));

                        onSuccess(data);
                        completedRequests++;
                        if (completedRequests == totalPages) {
                            createWordCloud(totalData);
                        } else {
                            getAllPages();
                        }
                    },
                    error: function (err) {
                        console.error('Word Cloud: a getData call failed, not all responses will be accounted for');

                        completedRequests++;
                        if (completedRequests == totalPages) createWordCloud(totalData);
                    }
                });

                // get data for all required pages
                function getAllPages() {
                    debugLog('total pages: ' + totalPages);

                    for (var i = 1; i < totalPages; i++) { // i = 1, start on page 2
                        getData({
                            request: chartConfig,
                            page: (i + 1) * interval,
                            pageSize: perPage,
                            success: function (data) {
                                onSuccess(data);
                                completedRequests++;

                                if (completedRequests == totalPages) createWordCloud(totalData);
                            },
                            error: function (err) {
                                console.error('Word Cloud: a getData call failed, not all responses will be accounted for');

                                completedRequests++;
                                if (completedRequests == totalPages) createWordCloud(totalData);
                            }
                        });
                    }
                }

                // callback on getData success - process DB2 return data into correct High Charts Word Cloud format
                function onSuccess(data) {
                    if (data.Ch[0].Ch[0].Vs !== undefined) {
                        // turn list of DB2 data into individual verbatim responses (strip unnecessary data)
                        data = data.Ch[0].Ch[0].Vs.map(function (d) {
                            return d.V
                        });

                        var tokenizer = XRegExp("[\\pL\\pN]+", "g");

                        data = data.forEach(function(verbatim) {
                            // remove ignored phrases
                            userConfig.ignorePhrases.forEach(function (regexp) {
                                verbatim = verbatim.replace(regexp, '');
                            });

                            // tokenize verbatim into words by word boundary
                            var words = XRegExp.match(verbatim, tokenizer);

                            words = words.forEach(function(w) {
                                // remove any all-non-word or empty/nonsense strings in case any got through
                                if (/^\s+$|^$|^[^\w]+$/.test(w)) return;

                                w = w.toLowerCase(); // all words to lower case for processing

                                // remove stop words
                                for (iw of stopwords) {
                                    if (w === iw) return;
                                }

                                // remove ignore words
                                for (iw of userConfig.ignoreWords) {
                                    if (typeof iw === 'string') {
                                        if (w === iw) return;
                                    } else if (iw instanceof RegExp) {
                                        if (iw.test(w)) return;
                                    }
                                }

                                // word passed all tests! Add word to totalData
                                wordIndex = totalData.findIndex(function (ev) {
                                    return w == ev.name
                                });
                                if (wordIndex !== -1) { // word already exists, increment weight
                                    totalData[wordIndex].weight++;
                                } else { // word doesn't exist, add it to totalData
                                    totalData.push({
                                        name: w,
                                        weight: 1
                                    });
                                }
                            });
                        });
                    }
                }
            }

            // callback to create the actual word cloud chart once data has been fetched
            function createWordCloud(totalData) {
                debugLog('creating word cloud');

                window[containerID + '_data'] = {
                    hash: stringToHash(JSON.stringify({
                        Flags: chartConfig.Flags,
                        SourceID: chartConfig.SourceID,
                        Children: chartConfig.Children,
                        Filters: chartConfig.Filters,
                        BeginDate: chartConfig.BeginDate,
                        EndDate: chartConfig.EndDate
                    })),
                    data: totalData
                };

                data = totalData;

                data = data.sort(function (a, b) {
                    return b.weight - a.weight;
                });

                data = data.slice(0, userConfig.maxWords);

                var cutOffI = (userConfig.maxWords > data.length? data.length : userConfig.maxWords) -1;
                var minWeight = data[cutOffI].weight;
                var maxWeight = data[0].weight;
                var makeScale = function makeScale() {
                    var rangeStart = userConfig.theme.opacityScale[0];
                    var rangeEnd = userConfig.theme.opacityScale[1];

                    return (value) => {
                        return rangeStart + (rangeEnd - rangeStart) * ((value - minWeight) / (maxWeight - minWeight));
                    }
                };
                var scale = makeScale();

                var colors = userConfig.highchartsConfigs.plotOptions.wordcloud.colors.map(function (c) {
                    return colorToRGBArray(c);
                });

                data = data.map(function (word) {
                    var color = colors[Math.floor(Math.random() * colors.length)];
                    return {
                        name: word.name,
                        weight: word.weight,
                        color: 'rgba(' + color.toString() + ',' + scale(word.weight) + ')'
                    };
                });

                debugLog('finished processing verbatims');
                debugLog(data);

                $.extend(true, userConfig.highchartsConfigs, {
                    series: [{
                        data: data
                    }]
                });

                // create Highcharts Word Cloud
                Highcharts.chart(containerID, userConfig.highchartsConfigs);

                debugLog('finished creading word cloud');

                spinner.hide();
            }
        }

        // Dashboard hooks

        var filterTemplate = { // default template for all filters required by getData
            ObjectType: '',
            Operator: '',
            Filters: [{
                ID: '',
                Action: '',
                Count: '',
                Description: '',
                Flags: '',
                Name: '',
                ObjectType: '',
                Operator: '',
                ParentID: '',
                ScaleType: '',
                SourceID: '',
                Values: [{
                    ObjectType: '',
                    ID: ''
                }],
                Value: ''
            }]
        };

        var chartTemplate = { // template for all charts required by getData
            Name: '',
            ObjectType: '',
            Description: function (description) {
                description = JSON.parse(description);
                description.chartType = 'table';
                return JSON.stringify(description);
            },
            LocalizationKey: '',
            Flags: function () { // have to manually add flags otherwise wrong amount of data is returned!
                var report = self.report;
                while (report.parentReport !== undefined) report = report.parentReport;

                if (report.model.get('BeginDate')) return Enums.Flags.DateRange;
            },
            GUID: '',
            ID: '',
            SourceID: '',
            Children: [{
                ID: '',
                Name: '',
                SourceID: '',
                ObjectType: '',
                ParentID: '',
                ScaleType: '',
                GUID: '',
                BeginDate: formatDate, // widget date => getData date needs formatting
                EndDate: formatDate, // widget date => getData date needs formatting
                Flags: '',
                LocalizationKey: '',
                Filters: [
                    filterTemplate
                ]
            }],
            Breakdowns: function () { // custom 'Respondents' breakdown - make getData return for a response-level table
                return [{
                    GUID: generateGUID(),
                    Name: 'Respondents',
                    SourceID: self.model.get('Children').first().get('SourceID'),
                    Action: 4,
                    ObjectType: 2,
                    Flags: 1
                }];
            },
            Filters: function (level) { // filters for whole chart
                var dashboardFilters = getFromTemplate([filterTemplate], self.dataTile.request.get('Filters')); // filters from datatile/outside the chart model
                var chartFilters = getFromTemplate([filterTemplate], level); // filters from chart

                $.extend(true, dashboardFilters, chartFilters); // clashing chart filters overwrite dashboard filters
                return dashboardFilters;
            },
            BeginDate: function (date) { // widget date => getData date needs formatting. Use dashboard level date if no chart date set
                return formatDate(date) || formatDate(self.dataTile.request.get('BeginDate'));
            },
            EndDate: function (date) { // widget date => getData date needs formatting. Use dashboard level date if no chart date set
                return formatDate(date) || formatDate(self.dataTile.request.get('EndDate'));
            }
        };

        function getFromTemplate(template, level) { // fill the templates with data taken from the chart/model
            if (level === undefined) return;

            if (typeof template === 'function') {
                var retVal = template(level);
                if (retVal !== undefined) return retVal;
            } else if (typeof template === 'object' && template.constructor === Object) {
                var returnObj = {};
                Object.keys(template).forEach(function (k) {
                    var retVal = getFromTemplate(template[k], level.get(k));
                    if (retVal !== undefined) returnObj[k] = retVal;
                });
                if (Object.keys(returnObj).length) return returnObj;
            } else if (typeof template === 'object' && template.constructor === Array) {
                var returnArr = level.reduce(function (acc, l) {
                    var retVal = getFromTemplate(template[0], l);
                    return retVal !== undefined ? acc.concat([retVal]) : acc;
                }, []);
                if (returnArr.length) return returnArr;
            } else if (template !== '' && template !== undefined) {
                return template;
            } else {
                return level;
            }
        }

        function getData(config) { // getData ajax. Requires config = {request, page, pageSize, 'success' callback, 'error' callback}
            $.ajax({
                type: 'POST',
                url: '/Reporting/GetData',
                dataType: 'json',
                data: [
                    'request=' + JSON.stringify(config.request),
                    'page=' + config.page,
                    'pageSize=' + config.pageSize,
                ].join('&'),
                beforeSend: function (request) {
                    spinner.show();
                    request.setRequestHeader('__RequestVerificationToken', document.querySelector('input[name="__RequestVerificationToken"]').value);
                    request.setRequestHeader("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
                },
                success: function (res) {
                    config.success(res);
                },
                error: function (res) {
                    config.error(res);
                }
            });
        }

        function getReport(callback) { // get report/dashboard ajax
            var rvtEl = document.querySelector('input[name="__RequestVerificationToken"]');
            if (!rvtEl || !rvtEl.value) return;

            var xhr = new XMLHttpRequest();
            xhr.open('GET', '/Reporting/GetReport?profile=&reportId=' + localStorage['lastReportIdLoaded']);

            xhr.setRequestHeader('__RequestVerificationToken', rvtEl.value);

            xhr.onreadystatechange = function () {
                if (this.readyState == XMLHttpRequest.DONE && this.status == 200) {
                    callback(JSON.parse(JSON.parse(this.responseText).Value));
                }
            }

            xhr.send();
        }

        function saveReport(report, callback) { // save report/dashboard ajax
            var rvtEl = document.querySelector('input[name="__RequestVerificationToken"]');
            if (!rvtEl || !rvtEl.value) return;

            var xhr = new XMLHttpRequest();
            xhr.open('POST', '/Reporting/SaveReport');

            xhr.setRequestHeader('__RequestVerificationToken', rvtEl.value);
            xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");

            xhr.onreadystatechange = function () {
                if (this.readyState == XMLHttpRequest.DONE && this.status == 200) {
                    callback(this.responseText);
                }
            }

            xhr.send('report=' + encodeURIComponent(JSON.stringify(report)) + '&isSnapshot=false&expirationDate=&companyName=&reportType=1');
        }

        function getValueStrings(sourceId, attributeId, callback) { // get report/dashboard ajax
            var rvtEl = document.querySelector('input[name="__RequestVerificationToken"]');
            if (!rvtEl || !rvtEl.value) return;

            var xhr = new XMLHttpRequest();
            xhr.open('POST', '/Reporting/GetValueStrings');

            xhr.setRequestHeader('__RequestVerificationToken', rvtEl.value);
            xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");

            xhr.onreadystatechange = function () {
                if (this.readyState == XMLHttpRequest.DONE && this.status == 200) {
                    callback(JSON.parse(this.responseText).values);
                }
            }

            xhr.send('sourceID=' + sourceId + '&attributeID=' + attributeId + '&ignoreAnalystFilter=false&dataType=0&profile=&loadFromCase=false');
        }

        function getChartFromReport(report, GUID) {
            function flatten(rep) { // flatten the report to expose all charts in a 1D array
                return rep.reduce(function (flat, toFlatten) {
                    if (toFlatten.Children) {
                        if (toFlatten.ObjectType == 9) {
                            return flat.concat(flatten(toFlatten.Children));
                        } else if (toFlatten.ObjectType < 9) {
                            return flat.concat(toFlatten.Children);
                        }
                    }
                    return flat;
                }, []);
            }

            var charts = flatten(report.Children);
            return charts.find(function (chart) {
                return chart.GUID === GUID;
            });
        }

        // Platform Booty

        var Enums = {
            'Flags': {
                'Undefined': 0,
                'Numeric': 1,
                'Categorical': 2,
                'Debug': 4,
                'IsSplit': 8,
                'Ascending': 16,
                'Descending': 32,
                'Deleted': 64,
                'NotApplicable': 128,
                'NoData': 256,
                'Refresh': 512,
                'TimeTrend': 1024,
                'Sort': 2048,
                'Limit': 4096,
                'DateRange': 8192,
                'StatisticallyHigher': 16384,
                'StatisticallyLower': 32768,
                'IgnoreAnalystFilters': 65536,
                'Shared': 131072,
                'Owner': 262144,
                'ResponseList': 524288,
                'Weighted': 1048576,
                'WeighingSchema': 2097152,
                'Collapsed': 4194304,
                'QuestionGroup': 8388608,
                'ManualSort': 16777216,
                'IsAdmin': 33554432,
                'ApplyAnalystFilters': 67108864,
                'IsSecondaryYAxis': 134217728,
                'IsSorted': 268435456,
                'CustomGranularity': 536870912,
                'StatSig': 1073741824
            }
        };

        function generateGUID() { // generate GUID the way DB2 does natively
            return "xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx".replace(/[xy]/g, function (e) {
                var t = 16 * Math.random() | 0;
                return ("x" == e ? t : 3 & t | 8).toString(16);
            });
        }

        // other helpers

        function formatDate(date) { // format date for getData call
            if (date && typeof date === 'object' && date.constructor === Date) {
                return [
                    date.getFullYear(),
                    date.getMonth() + 1,
                    date.getDate()
                ].join('-');
            }

            return date;
        }

        // convert string to 32bit integer 
        function stringToHash(string) {
            var hash = 0;

            if (string.length == 0) return hash;

            for (i = 0; i < string.length; i++) {
                char = string.charCodeAt(i);
                hash = ((hash << 5) - hash) + char;
                hash = hash & hash;
            }

            return hash;
        }

        // convert Hex to RGBA
        function colorToRGBArray(hex) {
            var c;
            if (/^#([A-Fa-f0-9]{3}){1,2}$/.test(hex)) {
                c = hex.substring(1).split('');
                if (c.length == 3) {
                    c = [c[0], c[0], c[1], c[1], c[2], c[2]];
                }
                c = '0x' + c.join('');
                return [(c >> 16) & 255, (c >> 8) & 255, c & 255].join(',');
            }
            var match = hex.match(/\((\d+)\s*,\s*(\d+)\s*,\s*(\d+)/);
            if (match) {
                return [match[1], match[2], match[3]];
            }

            return [0, 0, 0];
        }

        // show debug messages
        function debugLog(message) {
            if (userConfig.advanced.debug) {
                if (typeof message === 'string') console.log(containerID + ' debug: ' + message);
                else console.log(message);
            }
        }

        function errorLog(message) {
            if (typeof message === 'string') console.error(containerID + ' error: ' + message);
            else console.error(message);
        }

        setup();
    }
})();
