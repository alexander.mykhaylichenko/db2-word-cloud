# DB2 Word Cloud

DB2 Word Cloud is an extensible widget for Dashboards 2 allowing a word cloud to be rendered in the reporting area using verbatim data.

![Snapshot of two word clouds](snapshot.jpg?raw=true "Snapshot, two word clouds in a dashboard, one showing positive sentiment and the other negative")

> **Warning: Dashboard PDF exports or snapshots are not supported**

## Usage

For now, each word cloud has to be created manually using the extensibility widget provided by Dashboards. You must be a super admin to create these. See the steps below to set up a single word cloud on a dashboard.

### Setup

First, you will need to configure the settings for the word cloud. Below is an example configuration that you will need to modify to meet your requirements.

```Javascript
{
    maxWords: 50,
    targetSampleSize: 5000,
    ignoreWords: [/bank(ing|)/i, "branch"],
    ignorePhrases: [/make\s*experience\s*better/i, /make\s*experience\s*even\s*better/i],
    language: 'en',
    highchartsConfigs: {
        plotOptions: {
            wordcloud: {
                colors: ['#a8000b', '#008000']
            }
        }
    }
}
```

**These are all the currently supported configurations. See the example above as a reference**
- **maxWords**: must be an integer above 0. This is the maximum number of words the word cloud will show, if the size of the word cloud in the report will be small then a lower number like 50 is recommended, if it will be large and take up most of the page then up to 200 is recommended.
- **targetSampleSize**: must be an integer above 0. Imagine your dashboard with its current filters and date range shows hundreds of thousands of responses, the word cloud will have to fetch and process them all, this could take many minutes depending on your internet speed and isn't too healthy for the backend, so the word cloud only takes a sample from the whole data, this number controls the size of the sample. In the example it will take up to 5000 verbatim responses and process them to show the word cloud, if there are less than 5000 responses in the data then it will simply take all of them, if there are more than 5000 it will take blocks of responses at regular intervals throughout the data to get as full coverage as possible.
- **ignoreWords**: must be an array of strings or regular expressions. This is where you configure words that you don't want shown in the word cloud, for example if a particular word comes up too often and dominated the cloud but it's not relevant you may want to remove it. In the example above all the possible types of ignoreWords array items are shown - in the array is a regular expression which will match the word "bank" or "banking", then the next item is a string which will simply match the word "branch" (case sensitive). You may have as many items in this array as you like (within reason) and you may have an array of mixed strings and regexp or just one or the other. Note, the ignoreWords will be checked just after the word cloud tokenizes the data and before it processes each word, so each word is stil raw and has its original letter case, meaning the items in this array must account for a difference between "branch" and "Branch" or even "bRaNcH" (depending on how the respondent has written it), which is why regexp is recommended since case can be ignored if required and it is much cleaner anyway, furthermore you may want to only ignore "Branch" but not "branch" so the option is there.
-  **ignorePhrases**: must be an array of regular expressions. Before the verbatims are tokenized into words you also have the ability to remove entire phrases, in the example above the verbatims are combined into one via an internal process and some phrases are appended to each one, this makes the word cloud show these words with the largest text and completely ruins it, so there are two regular expressions that try to match these phrases "make experience better" and "make experience even better". Note, just like the ignoreWords these are case-sensitive and are checked against the raw respondent data, also no strings allowed here, only regexp!
- **language**: a string containing a valid ISO 369-1 language code (two letters): The word cloud can support multiple languages with lists of stop-words already implemented for each language, **but only one language at a time**, currently supported languages are given in the table below. Note, this cannot be changed once the word cloud has been created.

| language | ISO code |
| ------ | ------ |
| English | en |
| Spanish | es |
| French | fr |
| Italian | it |
| Malay | ms |

_see https://en.wikipedia.org/wiki/List_of_ISO_639-1_codes for more info_

- **highchartsConfigs**: an object of configuration parameters. This exposes the word clouds underlying Highcharts settings (it uses the Highcharts word cloud library), in the example the colour of the words have been configured to show the words in alternating red and green (super pretty, I know), this is normally the only visual parameter that needs to be set and in most cases it's only one colour, for one colour simply add only one colour item to the array, the colour needs to be a string with a hex code. If you are familiar with highcharts configurations then you are welcome to add more items to this object at your own risk.

> Hint: Multiple word clouds are allowed per dashboard


Now that the configuration options have been decided, it's time to prepare them for use, **in widget.js is the code you will need to paste into the "Javascript" section of the widget editor, open this file and add your required configuration into the "extraConfigs" variable at the top**, the example configurations are already in there for reference, when this is done you can proceed to the Installation section.

### Installation

This assumes you have already prepared your required configurations in the Setup section above.

1. Sign in as the owner of the dashboard you would like to add the word cloud to.
2. Enable the extensibility widget function by adding **?ext=1** into the URL after the domain, for example "...mcxplatform.de/?ext=1/#r/9e9bf3e9...", and hit enter.
3. Now you may drag in an extensibiltiy widget from the sidebar **Widgets > Extensible Content** to the desired position in your report
4. Open the widget's editor.
5. **First**, you need to add the data source, drag in the **Respondents** field from the correct source (survey) in the side panel. Note, you must select from the correct source because it cannot be changed later, you must destroy and rebuild the widget (or make another one) if you want to use a different source later.
6. In the **Text / Image** textbox add a single space.
7. **Lastly**, open the **Javascript** tab and paste in the prepared code with the configurations from the Setup section into the textbox (**the code from widget.js with your configuration pasted into it**).
8. Click Apply and wait. After a few seconds the widget will set itself up and show you an alert asking you to click OK. Click OK and the page will refresh. _if this does not happen see the Troubleshooting section_.
9. Now, if it worked the widget will now show a message like "a problem occured reading the data". Go into the widget's editor again, notice the Javascript tab has now disappeared (you can no longer edit the configuration... well, you can but you will need to edit the page's CSS). You can now remove the Respondents data field (which cannot be used in this type of chart, hence the warning message on the widget) and replace it with the desired verbatim, click apply when done. _If when you edit the widget you still see the Javascript tab, see the Troubleshooting section_.
10. Enjoy your new word cloud!

> Hint: If you want the word cloud to report on positive or negative sentiment, you will need to filter it by the Sentiment field setup by TA and use one word cloud per sentiment type.

## Troubleshooting

### On step 8 of the installation process I waited for ages but no alert ever showed up

Sometimes when adding Javascript to an extebsibility widget it does not get saved, this seems to happen mostly when changing all the settings in the widget editor too quickly. Just try steps 5 - 8 again, or check all of the things you did in those steps got saved when doing it the first time, if some things are missing just add them in again but with a couple of seconds or so between each step, and a wait a couple of seconds before you hit apply. Also, try to resist changing the name of the widget or making any other change during the installation process, you can change it after the word cloud shows correctly.

### On step 9 of the installation process I still see the Javascript tab

This is also related to the issue mentioned above, where the Javascript in extensibility widgets does not get saved, simply go through steps 5 - 8 again and correct anyy parts that did not get saved and don't change things too quickly.

### I have just created the word cloud without any issues but the spinner just keeps spinning and nothing shows

Check the **targetSampleSize** is not too large, it could be that there is simply a lot of data to fetch, try setting it to 5000 (by making a new word cloud) and see what happens. However, if you have just created the word cloud and the targetSampleSize is not large then try refreshing the page.

### I've been editing the word cloud, applying filters and such, but the word cloud just keeps showing the same data

If you have make many modifications to the widget or the widget's filters and date range it might be a little "overwhelmed", this is an issue we are looking into. After you make many changes it is advisable to refresh the page and let the word cloud fully reload.

### I can't get the word cloud to show in a PDF export or snapshot

Word clouds are not currently supported here.
