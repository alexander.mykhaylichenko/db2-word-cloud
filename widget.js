var extraConfigs = {
    maxWords: 50,
    targetSampleSize: 5000,
    ignoreWords: [/bank(ing|)/i, /branch/i],
    ignorePhrases: [/make\s*experience\s*better/i, /make\s*experience\s*even\s*better/i],
    language: 'en',
    highchartsConfigs: {
        plotOptions: {
            wordcloud: {
                colors: ['#a8000b', '#008000']
            }
        }
    }
};

if (!window.loadDB2WordCloud && !window.gettingWordCloudScript) {
    window.gettingWordCloudScript = true;
    $.getScript('https://platform-services.custom.eu.mcx.cloud/DB2Extensibility/wordcloud.js', function () {
        delete window.gettingWordCloudScript;
        loadDB2WordCloud(self, extraConfigs);
    });
} else if (window.gettingWordCloudScript) {
    var wait = setInterval(function() {
        if (window.loadDB2WordCloud) {
            clearInterval(wait)
            loadDB2WordCloud(self, extraConfigs);
        }
    }, 500);
} else {
    loadDB2WordCloud(self, extraConfigs);
}
